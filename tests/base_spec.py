import sys, json
sys.path.append('lib')
from mockito import when, mock, unstub
from expects import *
from mamba import description, context, it
import cndvra8
import requests


with description('Base') as self:
    with before.each:
        unstub()
        self.host = "https://vra.com"
        self.password = "password"
        self.user = "user"
        self.access_token = "acces_token"
        self.base = cndvra8._base.Base(self.host)

    with context("__init__"):
        with it("shoud set the token"):
            expect(self.base.host).to(equal(self.host))

    with context("_query"):
        with it("shoud raise an error if method is not allowed"):
            expect(lambda: self.base._query("", method="toto")).to(raise_error(AttributeError))

    with context("_query"):
        with it("shoud accept get by default and load the json"):
            response = mock({'content': '{"a": "b"}', 'status_code': 200}, spec=requests.Response)
            when(requests).get(...).thenReturn(response)
            result = self.base._query('url')
            expect(json.loads(result.content)).to(equal({"a": "b"}))

    with context("_query"):
        with it("shoud return False if result is not good"):
            response = mock({'content': '{"a": "b"}', 'status_code': 500}, spec=requests.Response)
            when(requests).get(...).thenReturn(response)
            result = self.base._query('url')
            expect(result).to(equal(False)) 
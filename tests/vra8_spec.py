import sys
sys.path.append('lib')
from mockito import when, mock, unstub
from expects import *
from mamba import description, context, it
import cndvra8


with description('Vra8'):
    with before.each:
        unstub()
        self.host = "https://vra.com"
        self.password = "password"
        self.user = "user"
        self.access_token = "acces_token"
        self.vra8 = cndvra8.Vra8(self.host)

    with context("__init__"):
        with it("shoud set the token"):
            expect(self.vra8.host).to(equal(self.host))

    with context("build_deployment_name"):
        with it("shoud return name"):
            result = self.vra8.build_deployment_name('deployid', 'service', 'env')
            expect(result).to(equal("vra_service-env-43b789c1"))

        with it("shoud always retrun same name"):
            result = self.vra8.build_deployment_name('deployid', 'service', 'env')
            expect(result).to(equal("vra_service-env-43b789c1"))

    with context("rename_deployment"):
        with before.each:
            when(self.vra8)._api_version(...).thenReturn("")

        with it("shoud return True when success"):
            response = mock({
                'status_code': 200,
                'content': 'toto'
            })
            when(self.vra8)._query(...).thenReturn(response)
            result = self.vra8.rename_deployment('deployid', 'new-name')
            expect(result).to(equal(True))

        with it("shoud return None when code not 200"):
            response = mock({
                'status_code': 300,
                'content': 'toto'
            })
            when(self.vra8)._query(...).thenReturn(response)
            result = self.vra8.rename_deployment('deployid', 'new-name')
            expect(result).to(equal(None))

        with it("shoud return None when code not 200"):
            when(self.vra8)._query(...).thenReturn(False)
            result = self.vra8.rename_deployment('deployid', 'new-name')
            expect(result).to(equal(None))
            
    # with context("connect"):
    #     with it("shoud return True if token is not empty"):
    #         self.vra8.access_token = "PasEmpty"
    #         result = self.vra8.connect()
    #         expect(result).to(equal(True))  

    #     with it("shoud return True if token is empty and creds are valid"):
    #         self.vra8.access_token = None
    #         when(self.vra8)._build_connection(...).thenReturn(True)
    #         result = self.vra8.connect()
    #         expect(result).to(equal(True))       

    #     with it("shoud return False if token is empty and creds are unvalid"):
    #         self.vra8.access_token = None
    #         when(self.vra8)._build_connection(...).thenReturn(False)
    #         result = self.vra8.connect()
    #         expect(result).to(equal(False))     